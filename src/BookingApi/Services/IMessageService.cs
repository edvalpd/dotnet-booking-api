﻿namespace BookingApi.Services
{
    public interface IMessageService
    {
        string GetMessage();
    }
}
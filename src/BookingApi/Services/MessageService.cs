﻿using System;

namespace BookingApi.Services
{
    public class MessageService : IMessageService
    {
        public string GetMessage()
        {
            return Environment.GetEnvironmentVariable("message");
        }
    }
}
using BookingApi.Maps;
using BookingApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookingApi
{
    public class BookingDbContext : DbContext
    {
        public BookingDbContext(DbContextOptions<BookingDbContext> options)
       : base(options)
        {

        }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Patient> Patients { get; set; }

        public DbSet<Appointment> Appointments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new RoomMap(modelBuilder.Entity<Room>());

            new PatientMap(modelBuilder.Entity<Patient>());

            EntityTypeBuilder<Appointment> entityBuilder = modelBuilder.Entity<Appointment>();
            entityBuilder
                                    .HasOne<Patient>(a => a.Patient)
                                    .WithMany(p => p.Appointments)
                                    .HasForeignKey(a => a.PatientId);

            entityBuilder
                                    .HasOne<Room>(a => a.Room)
                                    .WithMany(r => r.Appointments)
                                    .HasForeignKey(a => a.RoomId);

            new AppointmentMap(
            entityBuilder
            );

        }
    }
}
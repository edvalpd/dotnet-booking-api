using System.Collections.Generic;
using Newtonsoft.Json;

namespace BookingApi.Models
{
    public class Patient
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }


        // Used for the One-to-many mapping
        [JsonIgnore]
        public virtual ICollection<Appointment> Appointments { get; set; }

    }
}
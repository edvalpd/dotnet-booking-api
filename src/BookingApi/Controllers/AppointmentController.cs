﻿using BookingApi.Models;
using BookingApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BookingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {

        private readonly BookingDbContext _context;

        private readonly ILogger<AppointmentController> _logger;

        public AppointmentController(BookingDbContext context, ILogger<AppointmentController> logger)
        {
            _context = context;
            _logger = logger;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Appointment>>> GetAppointments()
        {
            return await _context.Appointments.ToListAsync();
        }


        [HttpPost]
        public async Task<ActionResult<Appointment>> CreateAppointment(AppointmentRequest appointmentRequest)
        {
            var bookingNum = (await _context.Appointments.CountAsync() + 1);

            Appointment appointment = new Appointment();
            appointment.BookingNumber = bookingNum;
            try
            {
                appointment.Patient = _context.Patients.Single(p => p.Code == appointmentRequest.PatientCode);
                appointment.Room = _context.Rooms.Single(r => r.Code == appointmentRequest.RoomCode);

                appointment.BookingStartDate = DateTime.ParseExact(appointmentRequest.BookingStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                appointment.BookingEndDate = DateTime.ParseExact(appointmentRequest.BookingEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (System.Exception)
            {
                // TODO implement better validation logic
                return BadRequest("Invalid request, please check the values for the appointment.");
            }
            appointment.RequestedFixedDuration = appointmentRequest.RequestedFixedDuration;
            appointment.RequestedExtraDuration = appointmentRequest.RequestedExtraDuration;

            _context.Appointments.Add(appointment);
            await _context.SaveChangesAsync();
            return Ok(await _context.Appointments.FindAsync(bookingNum));
        }

    }
}
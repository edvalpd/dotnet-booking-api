using System.ComponentModel.DataAnnotations;

namespace BookingApi.Controllers
{
    public class AppointmentRequest
    {
        [StringLength(10)]
        [Required()]
        public string PatientCode { get; set; }
        
        [StringLength(10)]
        [Required()]
        public string RoomCode { get; set; }

        [Required()]
        public string BookingStartDate { get; set; }

        [Required()]
        public string BookingEndDate { get; set; }

        public int RequestedFixedDuration { get; set; }
        public int RequestedExtraDuration { get; set; }

    }
}


### Install Git 

https://www.atlassian.com/git/tutorials/install-git 


### Install Docker Desktop 

https://www.docker.com/get-started 


### Install .NET Core 3.1 

https://dotnet.microsoft.com/download/dotnet-core/3.1 


### .NET Configuration

// From the project folder 

dotnet add src/BookingApi/BookingApi.csproj package Microsoft.AspNetCore.Mvc.NewtonsoftJson
dotnet add src/BookingApi/BookingApi.csproj package Npgsql.EntityFrameworkCore.PostgreSQL --version 3.1.4


### Docker 

#### Starting Docker 

// From the project folder  

docker-compose up -d 

#### Stoping Docker 

// From the project folder  

docker-compose down # If you use `docker-compose down -v`, any saved data will be lost!


#### Rebuilding / redeploying the booking api container

// From the project folder  

docker-compose up --force-recreate --no-deps -d --build booking_api 


### API Endpoints

http://localhost:7070/api/room - GET  

http://localhost:7070/api/patient - GET  

http://localhost:7070/api/appointment - GET  

http://localhost:7070/api/appointment - POST  

Request:  
`
{
    "roomCode": "R1",
    "patientCode": "P-A",
    "bookingStartDate": "31/07/2020",
    "bookingEndDate": "07/08/2020",
    "requestedFixedDuration": 2,
    "requestedExtraDuration": 5
}
`

Response:  
`
{
    "bookingNumber": 1,
    "patient": {
        "id": 1,
        "code": "P-A",
        "name": "Andrew",
        "appointments": []
    },
    "room": {
        "id": 1,
        "code": "R1",
        "description": "Room R1",
        "appointments": []
    },
    "patientId": 1,
    "roomId": 1,
    "bookingStartDate": "2020-07-31T00:00:00",
    "bookingEndDate": "2020-08-07T00:00:00",
    "requestedFixedDuration": 2,
    "requestedExtraDuration": 5
}
`

### Database 

All the information about the database can be found on the seed.sql file, also its access credentials can be found on the docker-compose.yml file.



